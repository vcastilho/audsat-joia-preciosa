package br.com.audsat.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.audsat.domains.NoDiscountCalculator;
import br.com.audsat.domains.Product;
import br.com.audsat.domains.WithDiscountCalculator;

@Service
public class ProductManagementService {

    Logger logger = LoggerFactory.getLogger(ProductManagementService.class);

    @Value("${discountPrice}")
    private double discountPrice;
    
    @Autowired
    private NoDiscountCalculator noDiscountCalculator;
    @Autowired
    private WithDiscountCalculator withDiscountCalculator;
    
    public double calculateSale(double price){
        logger.info("Calculando venda.");
        Product product = new Product(price);
        double sellPrice = noDiscountCalculator.calculateProductPrice(product);
        if(sellPrice > discountPrice){
            return withDiscountCalculator.calculateProductPrice(product);
        }
        return sellPrice;
    }

}
