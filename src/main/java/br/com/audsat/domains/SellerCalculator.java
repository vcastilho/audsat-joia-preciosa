package br.com.audsat.domains;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public abstract class SellerCalculator {

    @Value("${profit}")
    protected double profit;
    @Value("${comission}")
    protected double commission;
    
    public abstract double calculateProductPrice(Product product);

}
