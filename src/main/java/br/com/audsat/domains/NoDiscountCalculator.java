package br.com.audsat.domains;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NoDiscountCalculator extends SellerCalculator{


    @Override
    public double calculateProductPrice(Product product) {
        double sellPrice = product.getPrice() + product.getPrice() * profit;
        return sellPrice + sellPrice * commission;
    }
    
}
