package br.com.audsat.domains;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WithDiscountCalculator extends SellerCalculator {
    
    @Value("${discount}")
    private double discount;

    private double calculateDiscount(double sellPrice){
        return sellPrice * discount;
    }

    public double calculateProductPrice(Product product) {
        double sellPrice = product.getPrice() + product.getPrice() * profit;
        sellPrice += sellPrice * commission;
        return sellPrice - calculateDiscount(sellPrice);
    }
}
