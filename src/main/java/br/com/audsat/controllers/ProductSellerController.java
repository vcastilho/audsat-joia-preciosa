package br.com.audsat.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.audsat.services.ProductManagementService;

@Controller
public class ProductSellerController {

    Logger logger = LoggerFactory.getLogger(ProductSellerController.class);
    @Autowired
    private ProductManagementService productManagementService;

    @GetMapping("/calculate/{price}")
    public ResponseEntity<Double> calculateSale(@PathVariable("price") double price){
        logger.info("Calculando valor da venda.");
        return ResponseEntity.ok(productManagementService.calculateSale(price));
    }
    
}
